var gridfs = require('cadquarry.db.gridfs')
, logger = require('server.logging')
, async = require('async')
, _ = require('lodash')
, qs = require('qs')
, ms = require('ms')
, request = require('superagent')
, dwgReg = /^.+\.dwg/i
, search = require('redis-completer')
search = search('localhost', 6379)
search.applicationPrefix('phrases')
function strErr(err, res){
  logger.error(err, err.stack)
  return res.send(500)
}
var getFile = function(){}
getFile.prototype = {
  autocomplete: function(query, done){
    var self = this
    async.waterfall([
      self._initDb.bind(self),
      function(grid, done){
        self._autocomplete(query, done)
      }
      ], done)
  },
  _initDb: function(done){
    var self = this
    if(self.grid)return done(null, self.grid)
    gridfs('cadquarry_files', function(err, grid){
      if(err)return done(err)
      self.grid = grid
      done(null, grid)
    })
  },
  _autocomplete:function(query, done){
    // console.log(query);
    var self = this
    if(!query.q)return done(null)
      search.getPhraseCompletions(query.q, query.limit, function(err, results){
        // console.log(arguments);
        if(err)return done(err)
          // console.log(results);
          done(null, results)
      })
  }
}
function expiration(){
  return new Date(_.now()+ms('30m'))
}
var _getFile = new getFile()
, _expires = expiration()
var file = function(options){
  this.options = options||{}
  _.extend(this, getFile.prototype)
  setInterval(function(){
    _expires = expiration()
  }, ms('1h'))
}
file.prototype={
  middleware: function(req, res){
    res.setHeader('Expires', _expires)
    if(!req.query.q)return res.send(200)
    if(!_.isNumber(req.query.limit)&& req.query.limit)req.query.limit = parseInt(req.query.limit)
    req.query.limit = req.query.limit || 10
    if(!_.isNumber(req.query.skip)&& req.query.skip)req.query.skip = parseInt(req.query.skip)
    req.query.skip = req.query.skip || 0
    _getFile
    ._autocomplete(req.query, function(err, result){
      if(err&&err!=-1)return strErr(err, res)
        // console.log(result);
      res.send(result)
  })
}
}
module.exports = new file()