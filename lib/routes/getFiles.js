var gridfs = require('cadquarry.db.gridfs')
, logger = require('server.logging')
, async = require('async')
, _ = require('lodash')
, qs = require('qs')
, ms = require('ms')
, request = require('superagent')
, dwgReg = /^.+\.dwg/i
, search = require('redis-completer')
search = search('localhost', 6379)
search.applicationPrefix('phrases')
function strErr(err, res){
  logger.error(err, err.stack)
  return res.send(500)
}
var getFile = function(){}
getFile.prototype = {
  search: function(query, done){
    var self = this
    async.waterfall([
      self._initDb.bind(self),
      function(grid, done){
        self._search(query, done)
      },
      function(done){
        self._getPopular(query, done)
      }
      ], done)
  },
  _initDb: function(done){
    var self = this
    if(self.grid)return done(null, self.grid)
    gridfs('cadquarry_files', function(err, grid){
      if(err)return done(err)
      self.grid = grid
      done(null, grid)
    })
  },
  _search:function(query, done){
    var self = this
    if(!query.q)return done(null)
      search.search(query.q, query.limit, function(err, results){
        if(err)return done(err)
        results = _.map(results, function(r){
          r = r.split(':')
          r = {
            _id: r[0],
            filename: r[1],
            d: 0,
            v: 0
          }
          return r
        })
        results = _.uniq(results, '_id')
        query.count = results.length
        if(query.skip>0)
        results = _.rest(results, query.skip)
        results = _.first(results, query.limit)
        results = {
          results: results,
          count:query.count
        }
          done(-1, results)
      })
  },
  _getPopular: function(query, done){
    var self = this
    async.waterfall([
      function(done){
        self.grid.files.find({}, {filename: 1}).count(done)
      },
      function(count, done){
        query.count=count
        self._getPopularFromDb(query, done)
      }
      ], done)
  },
  _getPopularFromDb:function(query, done){
    // console.log(query);
    var self = this
    self.grid.files
    // .find({filename:{$regex: '*.dwg'}}, {filename: 1})
    // .find({filename:{$regex: dwgReg}}, {filename: 1})
    .find({}, {filename: 1})
    // .sort({"metadata.downloadCount": -1})
    .sort({"metadata.r": -1})
    .limit(query.limit)
    .skip(query.skip)
    .toArray(function(err, results){
        results = _.map(results, function(r){
          r.d = 0,
          r.v = 0
          return r
        })
      if(err)return done(err)
        results = {
          results: results,
          count: query.count
        }
        // console.log(result);
        done(null, results)
    })
  }
}
function expiration(){
  return new Date(_.now()+ms('30m'))
}
var _getFile = new getFile()
, _expires = expiration()
var file = function(options){
  this.options = options||{}
  _.extend(this, getFile.prototype)
  setInterval(function(){
    _expires = expiration()
  }, ms('1h'))
}
file.prototype={
  middleware: function(req, res){
    res.setHeader('Expires', _expires)
    if(!_.isNumber(req.query.limit)&& req.query.limit)req.query.limit = parseInt(req.query.limit)
    req.query.limit = req.query.limit || 10
    if(!_.isNumber(req.query.skip)&& req.query.skip)req.query.skip = parseInt(req.query.skip)
    req.query.skip = req.query.skip || 0
    if(!req.query.q)
    return _getFile
      .search(req.query, function(err, result){
        if(err&&err!=-1)return strErr(err, res)
        res.send(result)
    })
    _getFile
    ._search(req.query, function(err, result){
      if(err&&err!=-1)return strErr(err, res)
        // console.log(result);
      res.send(result)
  })
}
}
module.exports = new file()
