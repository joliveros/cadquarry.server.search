var assert = require('chai').assert
, request = require('supertest')
, ware = require('../index.js')
, express = require('express')
, app = express()
, qs = require('qs')
, _ = require('lodash')
, ms = require('ms')
, async = require('async')
, path = require('path')

app.use(express.json())
app.use(express.urlencoded())
app.use(express.cookieParser())
app.use(ware)

request = request(app)

suite( 'file api', function() {
  this.timeout(ms('10s'))
  var test_jpg = path.resolve('./test/test.jpg')
  , test_zip = path.resolve('./test/test.zip')
  , test_users = []
  function hasFields(task, expectedFields){
    return expectedFields.length > _.intersection(expectedFields, _.keys(task))
  }
  test( 'GET 200 /search?q=electrical%20switch&skip=0&limit=10', function(done) {
    var query = {
      q:'el',
      skip:0,
      limit: 10
    }
    var url = '/search?'+qs.stringify(query)
    request
    .get(url)
    .expect(200)
    .end(function(err, res){
      // console.log(res.body);
      done(null)
    })
  }); 
  test( 'GET 200 /search?skip=0&limit=30', function(done) {
    var query = {
      skip:0,
      limit: 30
    }
    var url = '/search?'+qs.stringify(query)
    request
    .get(url)
    .expect(200)
    .end(done)
  }); 
  test( 'GET 200 /search?q=', function(done) {
    var query = {
      q:''
    }
    var url = '/search?'+qs.stringify(query)
    request
    .get(url)
    .expect(200)
    .end(done)
  });
  test( 'GET 200 /search?q=', function(done) {
    var url = '/search'
    request
    .get(url)
    .expect(200)
    .end(done)
  });
  // test( '#_getPopular', function(done) {
  //   var getFiles = require('../lib/routes/getFiles')
  //   getFiles._initDb(function(err){
  //     getFiles._getPopular({limit: 10, skip: 0}, done)
  //   })
  // });
  test( '#_autocomplete', function(done) {
    var autocomplete = require('../lib/routes/autocomplete')
    autocomplete._initDb(function(err){
      autocomplete._autocomplete({limit: 10, skip: 0, q:'window'}, function(err, results){
        if(err)return done(err)
          done(assert.isArray(results))
      })
    })
  });
  test( 'GET 200 /search/autocomplete?skip=0&limit=30', function(done) {
    var query = {
      skip:0,
      limit: 6,
      q: 'tree'
    }
    var url = '/search/autocomplete'
    request
    .get(url)
    .query(query)
    .expect(200)
    .end(function(err, res){
      if(err)return done(err)
        // console.log(res.body);
      done(assert.isArray(res.body))
    })
  }); 
  setup(function(){
  });
  
  teardown(function(){
    
  });  
});