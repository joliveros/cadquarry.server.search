var express = require("express")
, NM = require('express-namespace')
, app = express()

  app.namespace('/search', function(){
    app.get('/', require('./lib/routes/getFiles').middleware)
    app.get('/autocomplete', require('./lib/routes/autocomplete').middleware)
  })
module.exports = app