var gridfs = require('cadquarry.db.gridfs')
, async = require('async')
, _ = require('lodash')
, completer = require('redis-completer')
_.str = require('underscore.string');

// Mix in non-conflict functions to Underscore namespace if you want
_.mixin(_.str.exports());

// All functions, include conflict, will be available through _.str object
_.str.include('Underscore.string', 'string'); // => true
completer = completer('localhost', 6379)
completer.applicationPrefix('phrases')
async.waterfall([
  function(done){
    gridfs('cadquarry_files', done)
  },
  function(grid, done){
    var count = 0
    , skip = 0
    async.doUntil(function(done){
      grid.files.find({}, {limit: 10, skip: skip}).toArray(function(err, results){
        count = results.length
        skip+=10
        async.each(results, processStrings, done)
        // done(null)
      })
    }, function(){return count==0}, done)
  }
  // function(grid, done){
  //   grid.files
  //   .distinct('metadata.strings'
  //   , function(err, results){
  //     if(err) throw err
  //     var keywords = []
  //     // _reg = /\n|\r/g
  //     , reg = /([^\n\r]+)/g
  //     results.forEach(function(r){
  //         var m = r.match(reg)
  //         if(!m)return keywords.push(r)
  //         m.forEach(function(a){
  //         a = _.trim(a, ' ')
  //         keywords.push(a)
  //         })
  //     })
  //     keywords = _.uniq(keywords)
  //     // console.log(keywords);
  //     keywords.forEach(function(k){

  //     })
  //     done(null, null)
  //   })
  // }
  ], function(err, results){
    process.exit()
  })
function processStrings(file, done){
  file.metadata = file.metadata||{}
  file = file.metadata
  file.strings = file.strings ||{}
  console.log(file.strings);
  done(null)
}